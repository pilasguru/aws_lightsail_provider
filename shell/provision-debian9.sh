#!/bin/sh
sleep 5
apt-get update || apt-get update
sleep 5
apt-get update || apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y apt-transport-https ca-certificates curl gnupg software-properties-common git
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install docker-ce
systemctl enable docker && systemctl start docker
curl https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker -o /etc/bash_completion.d/docker.sh
DEBIAN_FRONTEND=noninteractive apt-get install -y python-pip
pip install docker-compose

# Change NEWUSER name or set CREATEUSER to false
CREATEUSER=true
NEWUSER=rodolfo
if test "$CREATEUSER" = "true"; then
useradd -m -s /bin/bash -G docker $NEWUSER
echo "$NEWUSER ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-$NEWUSER
chmod 440 /etc/sudoers.d/90-$NEWUSER
mkdir /home/$NEWUSER/.ssh
cat > /home/$NEWUSER/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCkowdw4roqsXeTM95YXZGazbjZuO6zy/08VakcggnqDYqW7oXAaYLvtGeobdIw2X72P4pKxatBgQ1ump4kEk9PIiS0LOuL4bwApoyWzAztXzQw42N6e4Qw9833f1kZf+tvv/Vg8MuThXrpjqeHKrUwrnTxzaWsENrkR3P75SK0/l6ZQqNpFq79IbWZdUP6mtJWESl/oyF5NBa/X8rs2MsMbi8lEVQ+AknlIGNMrtc5wod+PQdHZnlFw6erjQcHAqTn1AYuv0OCOekAayVmiVWJME1CQx3us8q+5ypj8xHIeO22iOwfdY8WerAhaAog1Cn8PU17UJPSeGuRElTGLZVZ rodolfo@emilia
EOF
echo "alias ctop='docker run --rm -ti --name=ctop -v /var/run/docker.sock:/var/run/docker.sock quay.io/vektorlab/ctop:latest'"  >> /home/rodolfo/.bash_profile
chown -R $NEWUSER:$NEWUSER /home/$NEWUSER/.ssh /home/$NEWUSER/.bash_profile
fi

docker pull quay.io/vektorlab/ctop:latest
apt-get clean
