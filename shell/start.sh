#!/bin/bash

# aws lightsail get-blueprints --query 'blueprints[*].[blueprintId]' --output text
# aaws lightsail get-bundles --query 'bundles[*].[bundleId]' --output text

# atest	3.85.215.15	admin
function do_ssh {
	echo -n "Wait for ssh ${3}@${2} ."
	until ssh -o StrictHostKeyChecking=no -o  ConnectTimeout=1 ${3}@${2} exit > /dev/null 2>&1 ; do
		echo -n "."
		sleep 3
	done
	ssh -o StrictHostKeyChecking=no ${3}@${2}
}

aws lightsail create-instances \
	--instance-names atest \
	--availability-zone us-east-1a \
	--blueprint-id debian_9_5 \
	--bundle-id nano_2_0 \
	--key-pair-name id_rsa \
	--user-data file:///Users/rodolfo/Dev/LightSail/provision-debian9.sh

#	--user-data file:///Users/rodolfo/Dev/LightSail/cloud-init
#	--user-data file:///Users/rodolfo/Dev/Moove-it/sre-ansible/cloud-init/cloud-init-amazonlx.yaml

sleep 20

do_ssh $(aws lightsail get-instances --query 'instances[*].[name,publicIpAddress,username]' --output text)

exit
