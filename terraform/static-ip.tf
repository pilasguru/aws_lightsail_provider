resource "aws_lightsail_static_ip" "static-ip" {
  name = "${var.project_name}-ip"
}

resource "aws_lightsail_static_ip_attachment" "static-ip-attach" {
  static_ip_name = "${aws_lightsail_static_ip.static-ip.name}"
  instance_name  = "${aws_lightsail_instance.vm.name}"
}
