# Default keypair to be recordes into LightSail  
resource "aws_lightsail_key_pair" "key-pair" {
  name       = "${var.project_name}-key"
  public_key = "${file("${var.local_public_key}")}"
}

