
output "Access_SSH" {
	value = "ssh ${aws_lightsail_instance.vm.username}@${aws_lightsail_static_ip.static-ip.ip_address}"
}
