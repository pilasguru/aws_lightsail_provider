
variable "project_name" {
	description		= "Name of this project"
	default				= "aTest"
}

### Tags to use in your instance, you may add your own
### Default tags: projet_name 
locals {
    common_tags = "${map(
        "environment", "stage",
        "tag", "value"      
    )}"
}

### PROFILE for AWS credentials
variable "aws_profile" { 
	description		= "Profile from ~/.aws/credentials file"
	default 			= "default" 
}

### aws lightsail get-regions --query 'regions[*].[displayName,name]' --output text
variable "aws_region" {
	description 	= "Region to setup lightsail"
	default				= "us-east-1"
}

variable "aws_ls_availability_zone" { 
	description 	= "AWS region for hosting applications"
	default				= "us-east-1a" 
}

### aws lightsail get-blueprints --query 'blueprints[*].[blueprintId]' --output text
variable "aws_ls_blueprint" {
	description 	= "Base image build instance"
  #default       = "centos_7_1901_01"
	default				= "debian_9_5"
}

### aws lightsail get-bundles --query 'bundles[*].[bundleId]' --output text
variable "aws_ls_bundle" {
	description   = "Instance type to use for VM"
  default       = "nano_2_0"
  #default       = "micro_2_0"
  #default       = "small_2_0"
  #default       = "medium_2_0"
} 

### This is a local file
variable "local_user_data" {
	description		= "Local bash script user-data"
  #default       = "../shell/provision-centos7.sh"
	default				= "../shell/provision-debian9.sh"
}

### This is a local file
variable "local_public_key" {
	description		= "Local public key file"
	default				= "~/.ssh/id_rsa.pub"
}
