# Create a new GitLab Lightsail Instance
resource "aws_lightsail_instance" "vm" {
  name              = "${var.project_name}-instance"
  availability_zone = "${var.aws_ls_availability_zone}"
  blueprint_id      = "${var.aws_ls_blueprint}"
  bundle_id         = "${var.aws_ls_bundle}"
  key_pair_name     = "${var.project_name}-key"
  user_data         = "${file("${path.module}/${var.local_user_data}")}"
  tags = "${merge(
    local.common_tags,
    map(
        "setup", "terraform",
        "project", "${var.project_name}"
    )
  )}"
}
