# AWS Lightsail provider

This repo contains several ways to setup a basic infraestructure (one instance) with Lightsail AWS service & terraform:

* **shell/**: Bash scripting, together with *provision* shell scripts.
* **terraform/**: Terraform code

## Setup tools

### AWS cli

#### New setup

At your MacOS execute:

```
$ brew install awscli

$ aws configure
AWS Access Key ID [None]: <accesskey>
AWS Secret Access Key [None]: <secretkey>
Default region name [None]: us-east-2
Default output format [None]:
```

Verify with:

```
$ aws configure list
      Name                    Value             Type    Location
      ----                    -----             ----    --------
   profile                <not set>             None    None
access_key     ****************NDSC shared-credentials-file
secret_key     ****************aHNj shared-credentials-file
    region                us-east-2      config-file    ~/.aws/config
```

#### New profile (if there are others)

If you have configured aws with default profile and need to add one new:

```
$ aws configure --profile <profile>
AWS Access Key ID [None]: <accesskey>
AWS Secret Access Key [None]: <secretkey>
Default region name [None]: us-east-2
Default output format [None]:
```

Verify with:

```
$ aws configure list --profile <profile>
      Name                    Value             Type    Location
      ----                    -----             ----    --------
   profile                <profile>             None    None
access_key     ****************NDSC shared-credentials-file
secret_key     ****************aHNj shared-credentials-file
    region                us-east-2      config-file    ~/.aws/config
```

### Terraform

At your MacOS execute:

```
$ brew install terraform

$ terraform version
Terraform v0.12.9
+ provider.aws v2.29.0
```

## Setup terraform

To setup this project and deploy a infraestructure perform these steps:

```
$ git clone git@gitlab.com:pilasguru/aws_lightsail_provider.git
$ cd aws_lightsail_provider/terraform/
$ terraform init
```

Edit `variables.tf` file and configure to your needs.

```
variable "project_name" {
	description		= "Name of this project"
--
locals {
    common_tags = "${map(
    		"tag-name", "tag-value",

[...continue with each value ...]
```

Edit the provision shell script (i.e. `../shell/provision-debian7.sh`) or the script you have configured at `variable "local_user_data"` into `variables.tf` file, to fix new user to create shell code:

```
# Change NEWUSER name or set CREATEUSER to false
CREATEUSER=true
NEWUSER=rodolfo
```

also change the code to include your own `id_rsa.pub` key.

```
cat > /home/$NEWUSER/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAA                 << -----  YOUR KEY MUST BE HERE
EOF
```  

## Deploy infraestructure

```
$ terraform plan

$ terraform apply
```

## Delete infraestructure

```
terraform destroy --auto-approve
```

## License

[MIT License](LICENSE)

Copyright (c) 2019 Rodolfo Pilas

